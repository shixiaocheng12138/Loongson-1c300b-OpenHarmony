#include "los_config.h"
#include "los_debug.h"
#include "los_interrupt.h"
#include "los_task.h"
#include "los_tick.h"
#include "los_memory.h"

#include "ls1c_public.h"
#include "ls1c_gpio.h"
#include "ls1c_delay.h"
#include "ls1c_irq.h"

#define IFNAMSIZ 16
#include "lwip/sockets.h"
#include "lwip/lwipopts.h"

#define MAX_BUFFER 1024

// multi-threaded print
#define lock_printf(fmt, args...) do {  \
    LOS_TaskLock();                     \
    printf(fmt, ##args);                \
    LOS_TaskUnlock();                   \
} while (0)

extern int lwip_system_init(void);
extern int rt_hw_eth_init(void);

extern int ping(char* target_name, UINT32 times, UINT32 size);


UINT32 create_task(UINT32 *task_id, CHAR *task_name, TSK_ENTRY_FUNC task_entry, UINT32 arg)
{
    TSK_INIT_PARAM_S task = {0};

    task.pfnTaskEntry = task_entry;
    task.uwArg = arg;
    task.pcName = task_name;
    task.uwStackSize = 0x1000;
    task.usTaskPrio = 6; /* Os task priority is 6 */
    return LOS_TaskCreate(task_id, &task);
}

// ----------------PING test---------------------

void *ping_test(UINT32 arg)
{
    lock_printf("begin ping ...\r\n");
    ping("192.168.2.175", 4, 0);
    return NULL;
}

// ----------------UDP test-----------------------

void *udp_send_test(UINT32 arg)
{
    int fd = lwip_socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
    lock_printf("socket = %d\r\n", fd);
    if (fd == -1)
    {
        lock_printf("socket err = %d\r\n", fd);
        return NULL;
    }
    struct sockaddr_in dest_addr = {0};
    dest_addr.sin_family = AF_INET;         //ip v4
    dest_addr.sin_port = lwip_htons(9999);
    dest_addr.sin_addr.s_addr = ipaddr_addr(LOS_IPADDR); // ip addr

    char buffer[MAX_BUFFER];
    for (int i = 0; i < 1; i++) {
        sprintf_s(buffer, sizeof(buffer), "test udp send data %d.", i);
        lock_printf("sending udp data[%s]\r\n", buffer);
        LOS_TaskDelay(2000);
        lwip_sendto(fd, buffer, strlen(buffer), 0, (struct sockaddr *)&dest_addr, sizeof(dest_addr));

    }

    lock_printf("sending udp data[exit]\r\n");
    LOS_TaskDelay(2000);
    lwip_sendto(fd, "exit", 4, 0, (struct sockaddr *)&dest_addr, sizeof(dest_addr));

    lwip_close(fd);
    lock_printf("send udp test finish\r\n");

    return NULL;
}

void *udp_recv_test(UINT32 arg)
{
    int fd = lwip_socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
    lock_printf("socket = %d\r\n", fd);
    if (fd == -1)
    {
        lock_printf("socket err = %d\r\n", fd);
        return NULL;
    }
    struct sockaddr_in dest_addr = {0};
    dest_addr.sin_family = AF_INET;         //ip v4
    dest_addr.sin_port = lwip_htons(9999);
    dest_addr.sin_addr.s_addr = INADDR_ANY;

    int ret = lwip_bind(fd, (struct sockaddr *)&dest_addr, sizeof(dest_addr));
    if (ret < 0)
    {
        lock_printf("bind fail:%d\r\n", ret);
        lwip_close(fd);
        return NULL;
    }
    else
    {
        lock_printf("recv ready!!!\r\n");
    }

    struct sockaddr_in src_addr = {0};
    socklen_t len = sizeof(src_addr);
    char buf[MAX_BUFFER] = {0};

    while (1)
    {
        ret = lwip_recvfrom(fd, buf, sizeof(buf), 0, (struct sockaddr *)&src_addr, &len);
        if (ret == -1)
        {
            break;
        }
        lock_printf("udp recv from [%s:%d]: %s\r\n", inet_ntoa(src_addr.sin_addr), ntohs(src_addr.sin_port), buf);
        if (strcmp(buf, "exit") == 0)
        {
            break;
        }
        memset(buf, 0, sizeof(buf));
    }

    lwip_close(fd);
    lock_printf("recv udp test finish\r\n");

    return NULL;
}

// ----------------TCP test-----------------------
static int test_tcp_exit_flag(int add_flag)
{
    static int flag = 0;
    int t = 0;
    LOS_TaskLock();
    t = flag;
    flag = flag + add_flag;
    LOS_TaskUnlock();

    return t;
}
static int test_tcp_send(int sock, const char *send_data)
{
    lock_printf("socket = %d:send [%s].\r\n", sock, send_data);
    ssize_t ret = lwip_write(sock, send_data, strlen(send_data));
    if (ret < 0) {
        lock_printf("socket = %d:send error, close the connect.\r\n", sock);
    } else if (ret == 0) {
        lock_printf("socket = %d:send warning, send function return 0.\r\n", sock);
    } else {
        lock_printf("socket = %d:send success [%ld].\r\n", sock, ret);
    }

    return ret;
}

static int test_tcp_recv(int sock, char *recv_data, int size)
{
    int bytes_received = 0;
    fd_set readset_c;
    struct timeval timeout;
    
    timeout.tv_sec = 3;
    timeout.tv_usec = 0;

    char send_data[MAX_BUFFER];
    int index = 0;
    int ret = 0;
    while (1) {
        FD_ZERO(&readset_c);
        FD_SET(sock, &readset_c);

        if (lwip_select(sock + 1, &readset_c, NULL, NULL, &timeout) == 0) {
            continue;
        }

        bytes_received = lwip_read(sock, recv_data, size);
        if (bytes_received < 0)
        {
            lock_printf("sokcet = %d:received error, close the connect.\r\n", sock);
            ret = -1;
            break;
        }
        else if (bytes_received == 0)
        {
            lock_printf("sokcet = %d:received warning, recv function return 0.\r\n", sock);
            continue;
        }
        else
        {
            recv_data[bytes_received] = '\0';
            lock_printf("sokcet = %d:received [%s]\r\n", sock, recv_data);
            if (strcmp(recv_data, "exit") == 0)
            {
                ret = 1;
            }
            break;
        }

    }

    return ret;
}

void *tcp_client_test(UINT32 arg)
{
    LOS_TaskDelay(100);
    int fd = lwip_socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    lock_printf("socket = %d\r\n", fd);
    if (fd == -1)
    {
        return NULL;
    }
    struct sockaddr_in server_addr = {0};
    server_addr.sin_family = AF_INET;         //ip v4
    server_addr.sin_port = lwip_htons(9998);
    server_addr.sin_addr.s_addr = ipaddr_addr(LOS_IPADDR);
    memset(&(server_addr.sin_zero), 0, sizeof(server_addr.sin_zero));
    int ret = connect(fd, (struct sockaddr *)&server_addr, sizeof(struct sockaddr_in));
    if (ret != 0)
    {
        lock_printf("connect fail(%d)!\r\n", ret);
        lwip_close(fd);
        test_tcp_exit_flag(1);
        return NULL;
    }
    char recv_data[MAX_BUFFER];
    char send_data[MAX_BUFFER];

    for (int i = 0; i < 1; ++i) {
        sprintf_s(send_data, sizeof(send_data), "test tcp client(%u) send data %d.", arg, i);
        LOS_TaskDelay(3000);
        test_tcp_send(fd, send_data);
        test_tcp_recv(fd, recv_data, sizeof(recv_data));
    }

    LOS_TaskDelay(3000);
    test_tcp_send(fd, "exit");
    LOS_TaskDelay(3000);

    lwip_close(fd);
    test_tcp_exit_flag(1);
    lock_printf("send tcp client(%u) test finish\r\n", arg);

    return NULL;
}

void *tcp_server_accept_connect_test(UINT32 arg)
{
    int connected = (int) arg;
    char recv_data[MAX_BUFFER] = {0};
    char send_data[MAX_BUFFER];
    int index = 0;
    while (1) {

        int ret = test_tcp_recv(connected, recv_data, sizeof(recv_data));
        if (ret == 1) {
            // exit
            break;
        }

        sprintf_s(send_data, sizeof(send_data), "tcp server accept (%d) reply data %d.", connected, ++index);

        ret = test_tcp_send(connected, send_data);

        if (ret < 0) {
            // error
            break;
        }
    }

    lwip_close(connected);
    test_tcp_exit_flag(1);
    lock_printf("tcp server accept (%d) connect test finish\r\n", connected);

    return NULL;
}

void *tcp_server_test(UINT32 arg)
{
    int fd = lwip_socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    lock_printf("socket = %d\r\n", fd);
    if (fd == -1)
    {
        return NULL;
    }
    struct sockaddr_in server_addr = {0};
    server_addr.sin_family = AF_INET;         //ip v4
    server_addr.sin_port = lwip_htons(9998);
    server_addr.sin_addr.s_addr = INADDR_ANY;
    memset(&(server_addr.sin_zero), 0x0, sizeof(server_addr.sin_zero));

    int ret = lwip_bind(fd, (struct sockaddr *)&server_addr, sizeof(server_addr));
    if (ret < 0)
    {
        lock_printf("bind fail:%d\r\n", ret);
        lwip_close(fd);
        return NULL;
    }

    ret = lwip_listen(fd, 10);
    if (ret != 0)
    {
        lock_printf("listen error:%d\r\n", ret);
        lwip_close(fd);
        return NULL;
    }

    lock_printf("\r\ntcp server waiting for client on port %d...\r\n", 9998);

    struct sockaddr_in client_addr = {0};
    int client_addr_size = sizeof(client_addr);
    char buf[MAX_BUFFER] = {0};
    fd_set readset;
    struct timeval timeout;
    
    timeout.tv_sec = 3;
    timeout.tv_usec = 0;
    // test 4 tasks
    while (test_tcp_exit_flag(0) < 4)
    {
        FD_ZERO(&readset);
        FD_SET(fd, &readset);

        if (lwip_select(fd + 1, &readset, NULL, NULL, &timeout) == 0) {
            continue;
        }
        
        int connected = lwip_accept(fd, (struct sockaddr *)&client_addr, &client_addr_size);
        if (connected < 0)
        {
            lock_printf("accept connection failed! errno = %d\r\n", errno);
            continue;
        }

        lock_printf("I got a connection from (%s , %d) socket = %d\r\n", inet_ntoa(client_addr.sin_addr), ntohs(client_addr.sin_port), connected);

        UINT32 task_id = 0;
        char name[64];
        sprintf_s(name, sizeof(name), "tcp_socket_task_%d", connected);
        UINT32 taskret = create_task(&task_id, name, tcp_server_accept_connect_test, connected);
        if (taskret != LOS_OK) {
            lock_printf("create task error %u\r\n", taskret);
            break;
        }
        lock_printf("waiting for a new connection...\r\n");
    }

    lwip_close(fd);
    lock_printf("tcp server test finish\r\n");

    return NULL;
}

void create_socket_task(void)
{
    if (0 != lwip_system_init())
    {
        PRINT_ERR("init lwip failed ...\n");
        return;
    }
    PRINT_INFO("init lwip success ...\n");
    if (0 != rt_hw_eth_init())
    {
        PRINT_ERR("init eth failed ...\n");
        return;
    }
    PRINT_INFO("init eth success ...\n");

#if 1
    // ping test
    UINT32 task_id0;
    CHAR task_name0[] = "ping";
    if (create_task(&task_id0, task_name0, ping_test, 0) != LOS_OK) {
        lock_printf("create ping task error\r\n");
        return;
    }
    lock_printf("create ping task success\r\n");
#endif

#if 1
    // udp test
    UINT32 task_id1;
    CHAR task_name1[] = "udp_send";
    if (create_task(&task_id1, task_name1, udp_send_test, 0) != LOS_OK) {
        lock_printf("create udp send task error\r\n");
        return;
    }
    lock_printf("create udp send task success\r\n");

    UINT32 task_id2;
    CHAR task_name2[] = "udp_recv";
    if (create_task(&task_id2, task_name2, udp_recv_test, 0) != LOS_OK) {
        lock_printf("create udp recv task error\r\n");
        return;
    }
    lock_printf("create udp recv task success\r\n");
#endif
#if 1
    // tcp test
    UINT32 task_id3;
    CHAR task_name3[] = "tcp_server";
    if (create_task(&task_id3, task_name3, tcp_server_test, 0) != LOS_OK) {
        lock_printf("create tcp server test error\r\n");
        return;
    }
    lock_printf("create tcp server test success\r\n");
    
    UINT32 task_id4;
    CHAR task_name4[] = "tcp_client1";
    if (create_task(&task_id4, task_name4, tcp_client_test, 1) != LOS_OK) {
        lock_printf("create tcp client test 1 error\r\n");
        return;
    }
    lock_printf("create tcp client test 1 success\r\n");
    UINT32 task_id5;
    CHAR task_name5[] = "tcp_client2";
    if (create_task(&task_id5, task_name5, tcp_client_test, 2) != LOS_OK) {
        lock_printf("create tcp client test 2 error\r\n");
        return;
    }
    lock_printf("create tcp client test 2 success\r\n");
#endif
}

