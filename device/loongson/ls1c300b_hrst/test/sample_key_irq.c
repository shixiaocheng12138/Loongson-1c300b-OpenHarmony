#include "los_config.h"
#include "los_debug.h"
#include "los_interrupt.h"
#include "los_task.h"
#include "los_tick.h"

#include "ls1c_public.h"
#include "ls1c_gpio.h"
#include "ls1c_delay.h"
#include "ls1c_irq.h"

// 按键中断的标志
volatile int __gpio_key_irq_flag = 0;
void led_init(unsigned int led_gpio);

void __test_gpio_key_irqhandler(int IRQn, void *param)
{
    __gpio_key_irq_flag = 1;
}


VOID key_irq_task(VOID)
{
    unsigned int key_gpio = 85;     // GPIO85/I2C_SDA0
    int key_irq = LS1C_GPIO_TO_IRQ(key_gpio);
    static int count = 0;

    gpio_init(key_gpio, gpio_mode_input);
    gpio_set_irq_type(key_gpio, IRQ_TYPE_LEVEL_LOW);

    HalHwiCreate(key_irq, 0, 0, __test_gpio_key_irqhandler, NULL);

    printf("___>>>> start task %s\r\n", __FUNCTION__);

    while (1) {

        LOS_TaskDelay(100);

        if (1 == __gpio_key_irq_flag)
        {
            printf("___>>>>>> TaskSampleEntry1 %s %d\r\n", __FILE__, __LINE__);

        	__gpio_key_irq_flag = 0;      
        	count++;                
        	printf("[%s] count=%d, key press\r\n", __FUNCTION__, count);
        }
    }
}


void create_key_irq_task(void)
{
    UINT32 uwRet;
    UINT32 taskID1;
    TSK_INIT_PARAM_S stTask = {0};

    stTask.pfnTaskEntry = (TSK_ENTRY_FUNC)key_irq_task;
    stTask.uwStackSize = 0x1000;
    stTask.pcName = "gpio_task";
    stTask.usTaskPrio = 6; /* Os task priority is 6 */
    uwRet = LOS_TaskCreate(&taskID1, &stTask);
    if (uwRet != LOS_OK) {
        printf("key_irq_task create failed\r\n");
    }
}

