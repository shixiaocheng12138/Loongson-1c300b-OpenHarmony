#ifndef _LWIP_ADAPTER_LWIPOPTS_H_
#define _LWIP_ADAPTER_LWIPOPTS_H_

#include_next "lwip/lwipopts.h"

#undef LWIP_DHCP
#define LWIP_DHCP                       0 // 关闭DHCP功能
#undef LWIP_DEBUG
#define LWIP_DEBUG 1
#define LWIP_PROVIDE_ERRNO

#define LOS_IPADDR "192.168.1.176"
#define LOS_GWADDR "192.168.1.2"
#define LOS_MSKADDR "255.255.0.0"

#endif /* _LWIP_ADAPTER_LWIPOPTS_H_ */
