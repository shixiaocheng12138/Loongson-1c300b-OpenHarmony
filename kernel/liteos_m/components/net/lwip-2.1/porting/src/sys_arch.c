/*
 * Copyright (c) 2013-2019 Huawei Technologies Co., Ltd. All rights reserved.
 * Copyright (c) 2020-2021 Huawei Device Co., Ltd. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 *    conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list
 *    of conditions and the following disclaimer in the documentation and/or other materials
 *    provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used
 *    to endorse or promote products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <arch/sys_arch.h>
#include <lwip/sys.h>
#include <lwip/debug.h>
#include <los_task.h>
#include <los_tick.h>
#include <los_queue.h>
#include <los_sem.h>
#include <los_mux.h>
#include "cmsis_os2.h"
#include <los_tick.h>
#include <los_config.h>
#include <los_debug.h>
#include <lwip/inet.h>

#ifndef LOSCFG_KERNEL_SMP
#define LOSCFG_KERNEL_SMP 0
#endif

#if (LOSCFG_KERNEL_SMP)
SPIN_LOCK_INIT(arch_protect_spin);
static u32_t lwprot_thread = LOS_ERRNO_TSK_ID_INVALID;
static int lwprot_count = 0;
#endif /* LOSCFG_KERNEL_SMP */

#define ROUND_UP_DIV(val, div) (((val) + (div) - 1) / (div))
#define LWIP_LOG_BUF_SIZE 256

/**
 * Thread and System misc
 */
sys_thread_t sys_thread_new(const char *name, lwip_thread_fn thread, void *arg, int stackSize, int prio)
{
    UINT32 taskID = LOS_ERRNO_TSK_ID_INVALID;
    UINT32 ret;
    TSK_INIT_PARAM_S task = {0};

    /* Create host Task */
    task.pfnTaskEntry = (TSK_ENTRY_FUNC)thread;
    task.uwStackSize = stackSize;
    task.pcName = (char *)name;
    task.usTaskPrio = prio;
    task.uwArg = (UINTPTR)arg;
    task.uwResved = LOS_TASK_STATUS_DETACHED;
    ret = LOS_TaskCreate(&taskID, &task);
    if (ret != LOS_OK) {
        LWIP_DEBUGF(SYS_DEBUG, ("sys_thread_new: LOS_TaskCreate error %u\n", ret));
        return -1;
    }

    return taskID;
}

void sys_init(void)
{
    /* set rand seed to make random sequence diff on every startup */
    UINT32 seedlsb = (UINT32)LOS_SysCycleGet();
    srand(seedlsb);
}

extern UINT64 HalGetSysTick(void);

u32_t sys_now(void)
{
    /* Lwip docs mentioned like wraparound is not a problem in this funtion */
    // return (u32_t)osKernelGetTickCount();
    // uint64_t ticks = LOS_TickCountGet();
    UINT64 ticks = HalGetSysTick();
    return (u32_t)ticks;
}

/**
 * Protector
 */
sys_prot_t sys_arch_protect(void)
{
#if (LOSCFG_KERNEL_SMP)
    /* Note that we are using spinlock instead of mutex for LiteOS-SMP here:
     * 1. spinlock is more effective for short critical region protection.
     * 2. this function is called only in task context, not in interrupt handler.
     *    so it's not needed to disable interrupt.
     */
    if (lwprot_thread != LOS_CurTaskIDGet()) {
        /* We are locking the spinlock where it has not been locked before
         * or is being locked by another thread */
        LOS_SpinLock(&arch_protect_spin);
        lwprot_thread = LOS_CurTaskIDGet();
        lwprot_count = 1;
    } else {
        /* It is already locked by THIS thread */
        lwprot_count++;
    }
#else
    LOS_TaskLock();
#endif /* LOSCFG_KERNEL_SMP */
    return 0; /* return value is unused */
}

void sys_arch_unprotect(sys_prot_t pval)
{
    LWIP_UNUSED_ARG(pval);
#if (LOSCFG_KERNEL_SMP)
    if (lwprot_thread == LOS_CurTaskIDGet()) {
        lwprot_count--;
        if (lwprot_count == 0) {
            lwprot_thread = LOS_ERRNO_TSK_ID_INVALID;
            LOS_SpinUnlock(&arch_protect_spin);
        }
    }
#else
    LOS_TaskUnlock();
#endif /* LOSCFG_KERNEL_SMP */
}

/**
 * MessageBox
 */
err_t sys_mbox_new(sys_mbox_t *mbox, int size)
{
    CHAR qName[] = "lwIP";
    UINT32 ret = LOS_QueueCreate(qName, (UINT16)size, mbox, 0, sizeof(void *));
    switch (ret) {
        case LOS_OK:
            return ERR_OK;
        case LOS_ERRNO_QUEUE_CB_UNAVAILABLE:
        case LOS_ERRNO_QUEUE_CREATE_NO_MEMORY:
            return ERR_MEM;
        default:
            break;
    }
    LWIP_DEBUGF(SYS_DEBUG, ("%s: LOS_QueueCreate error %u\n", __FUNCTION__, ret));
    return ERR_ARG;
}

void sys_mbox_post(sys_mbox_t *mbox, void *msg)
{
    /* Caution: the second parameter is NOT &msg */
    UINT32 ret = LOS_QueueWrite(*mbox, msg, sizeof(char *), LOS_WAIT_FOREVER);
    if (ret != LOS_OK) {
        LWIP_DEBUGF(SYS_DEBUG, ("%s: LOS_QueueWrite error %u\n", __FUNCTION__, ret));
    }
}

err_t sys_mbox_trypost(sys_mbox_t *mbox, void *msg)
{
    /* Caution: the second parameter is NOT &msg */
    UINT32 ret = LOS_QueueWrite(*mbox, msg, sizeof(char *), 0);
    switch (ret) {
        case LOS_OK:
            return ERR_OK;
        case LOS_ERRNO_QUEUE_ISFULL:
            return ERR_MEM;
        default:
            break;
    }
    LWIP_DEBUGF(SYS_DEBUG, ("%s: LOS_QueueWrite error %u\n", __FUNCTION__, ret));
    return ERR_ARG;
}

err_t sys_mbox_trypost_fromisr(sys_mbox_t *mbox, void *msg)
{
    return ERR_ARG;
}

u32_t sys_arch_mbox_fetch(sys_mbox_t *mbox, void **msg, u32_t timeoutMs)
{
    void *ignore = 0; /* if msg==NULL, the fetched msg should be dropped */
    UINT64 tick = ROUND_UP_DIV((UINT64)timeoutMs * LOSCFG_BASE_CORE_TICK_PER_SECOND, OS_SYS_MS_PER_SECOND);
    UINT32 ret = LOS_QueueRead(*mbox, msg ? msg : &ignore, sizeof(void *), tick ? (UINT32)tick : LOS_WAIT_FOREVER);
    switch (ret) {
        case LOS_OK:
            return ERR_OK;
        case LOS_ERRNO_QUEUE_ISEMPTY:
        case LOS_ERRNO_QUEUE_TIMEOUT:
            return SYS_ARCH_TIMEOUT;
        default:
            break;
    }
    LWIP_DEBUGF(SYS_DEBUG, ("%s: LOS_QueueRead error %u\n", __FUNCTION__, ret));
    return SYS_ARCH_TIMEOUT; /* Errors should be treated as timeout */
}

u32_t sys_arch_mbox_tryfetch(sys_mbox_t *mbox, void **msg)
{
    void *ignore = 0; /* if msg==NULL, the fetched msg should be dropped */
    UINT32 ret = LOS_QueueRead(*mbox, msg ? msg : &ignore, sizeof(void *), 0);
    switch (ret) {
        case LOS_OK:
            return ERR_OK;
        case LOS_ERRNO_QUEUE_ISEMPTY:
            return SYS_MBOX_EMPTY;
        case LOS_ERRNO_QUEUE_TIMEOUT:
            return SYS_ARCH_TIMEOUT;
        default:
            break;
    }
    LWIP_DEBUGF(SYS_DEBUG, ("%s: LOS_QueueRead error %u\n", __FUNCTION__, ret));
    return SYS_MBOX_EMPTY; /* Errors should be treated as timeout */
}

void sys_mbox_free(sys_mbox_t *mbox)
{
    (void)LOS_QueueDelete(*mbox);
}

int sys_mbox_valid(sys_mbox_t *mbox)
{
    QUEUE_INFO_S queueInfo;
    return LOS_OK == LOS_QueueInfoGet(*mbox, &queueInfo);
}

void sys_mbox_set_invalid(sys_mbox_t *mbox)
{
    *mbox = LOSCFG_BASE_IPC_QUEUE_LIMIT;
}

/**
 * Semaphore
 */
err_t sys_sem_new(sys_sem_t *sem, u8_t count)
{
    UINT32 ret = LOS_SemCreate(count, sem);
    if (ret != LOS_OK) {
        return ERR_ARG;
    }

    return ERR_OK;
}

void sys_sem_signal(sys_sem_t *sem)
{
    (void)LOS_SemPost(*sem);
}

u32_t sys_arch_sem_wait(sys_sem_t *sem, u32_t timeoutMs)
{
    UINT64 tick = ROUND_UP_DIV((UINT64)timeoutMs * LOSCFG_BASE_CORE_TICK_PER_SECOND, OS_SYS_MS_PER_SECOND);
    UINT32 ret = LOS_SemPend(*sem, tick ? (UINT32)tick : LOS_WAIT_FOREVER); // timeoutMs 0 means wait forever
    switch (ret) {
        case LOS_OK:
            return ERR_OK;
        case LOS_ERRNO_SEM_TIMEOUT:
            return SYS_ARCH_TIMEOUT;
        default:
            break;
    }
    LWIP_DEBUGF(SYS_DEBUG, ("%s: LOS_SemPend error %u\n", __FUNCTION__, ret));
    return SYS_ARCH_TIMEOUT; /* Errors should be treated as timeout */
}

void sys_sem_free(sys_sem_t *sem)
{
    (void)LOS_SemDelete(*sem);
}

int sys_sem_valid(sys_sem_t *sem)
{
    return *sem != LOSCFG_BASE_IPC_SEM_LIMIT;
}

void sys_sem_set_invalid(sys_sem_t *sem)
{
    *sem = LOSCFG_BASE_IPC_SEM_LIMIT;
}

/**
 * Mutex
 */
err_t sys_mutex_new(sys_mutex_t *mutex)
{
    UINT32 ret = LOS_MuxCreate(mutex);
    if (ret != LOS_OK) {
        return ERR_ARG;
    }

    return ERR_OK;
}

void sys_mutex_lock(sys_mutex_t *mutex)
{
    (void)LOS_MuxPend(*mutex, LOS_WAIT_FOREVER);
}

void sys_mutex_unlock(sys_mutex_t *mutex)
{
    (void)LOS_MuxPost(*mutex);
}

void sys_mutex_free(sys_mutex_t *mutex)
{
    (void)LOS_MuxDelete(*mutex);
}

int sys_mutex_valid(sys_mutex_t *mutex)
{
    return *mutex != LOSCFG_BASE_IPC_MUX_LIMIT;
}

void sys_mutex_set_invalid(sys_mutex_t *mutex)
{
    *mutex = LOSCFG_BASE_IPC_MUX_LIMIT;
}

void LwipLogPrintf(const char *fmt, ...)
{
    if ((fmt == NULL) || (strlen(fmt) == 0)) {
        return;
    }

    int len;
    char buf[LWIP_LOG_BUF_SIZE] = {0};
    va_list ap;
    va_start(ap, fmt);
    len = vsprintf_s(buf, sizeof(buf) - 1, fmt, ap);
    va_end(ap);
    if (len < 0) {
        LWIP_LOGGER("log param invalid or buf is not enough.");
        return;
    }
    LWIP_LOGGER(buf);
}

/******************************************************************************
 * 
 * 
 *              added by liweibin
 * 
 * 
 * 
 * ***************************************************************************/
#if 1
#include "los_sem.h"

#include "ethernetif.h"
#include "etharp.h"
//#include "tcpip.h"
//#include "init.h"

extern int eth_system_device_init(void);
extern struct eth_device *HalGetEthDev(void);
#if LWIP_TCPIP_CORE_LOCKING
extern sys_mutex_t lock_tcpip_core;
#define LOCK_TCPIP_CORE() sys_mutex_lock(&lock_tcpip_core)
#define UNLOCK_TCPIP_CORE() sys_mutex_unlock(&lock_tcpip_core)
#else
#error "macro LWIP_TCPIP_CORE_LOCKING should enable ..."
#endif

typedef struct
{
    UINT32 wait_done;
    struct eth_device *ethif;
}TcpipCBParam;

extern err_t  tcpip_input(struct pbuf *p, struct netif *inp);

/*
 * Initialize the network interface device
 *
 * @return the operation status, ERR_OK on OK, ERR_IF on error
 */
static err_t netif_device_init(struct netif *netif)
{
    struct eth_device *ethif;

    ethif = (struct eth_device *)netif->state;
    if (ethif != NULL)
    {
        rt_device_t device;

        /* get device object */
        device = (rt_device_t) ethif;
        //if (rt_device_init(device) != RT_EOK)
        if (RT_EOK != device->init(device)) {
            return ERR_IF;
        }

        /* copy device flags to netif flags */
        netif->flags = ethif->flags;
        netif->mtu = ETHERNET_MTU;

        /* set output */
        netif->output = etharp_output;

        return ERR_OK;
    }

    return ERR_IF;
}
/*
 * Initialize the ethernetif layer and set network interface device up
 */
static void tcpip_init_done_callback(void *arg)
{
    TcpipCBParam *param = (TcpipCBParam *)arg;
    struct eth_device *ethif;
    ip4_addr_t ipaddr, netmask, gw;

    LWIP_ASSERT("invalid arg.\n", arg);

    IP4_ADDR(&gw, 0, 0, 0, 0);
    IP4_ADDR(&ipaddr, 0, 0, 0, 0);
    IP4_ADDR(&netmask, 0, 0, 0, 0);

    ethif = (struct eth_device *)param->ethif;

    LOS_SemPost(param->wait_done);

    return;
    LOCK_TCPIP_CORE();

    netif_add(ethif->netif, &ipaddr, &netmask, &gw,
              ethif, netif_device_init, tcpip_input);

    if (netif_default == NULL) {
        netif_set_default(ethif->netif);
    }

#if LWIP_DHCP
    /* set interface up */
    netif_set_up(ethif->netif);
    /* if this interface uses DHCP, start the DHCP client */
    dhcp_start(ethif->netif);
#else
    /* set interface up */
    netif_set_up(ethif->netif);
#endif

    if (ethif->flags & ETHIF_LINK_PHYUP) {
        netif_set_link_up(ethif->netif);
    }

    UNLOCK_TCPIP_CORE();

    //rt_sem_release(param->wait_done);
    LOS_SemPost(param->wait_done);
}

/**
 * LwIP system initialization
 */
int lwip_system_init(void)
{
    rt_err_t rc;
    UINT32 done_sem;
    TcpipCBParam cbParam;
    static int init_ok = 0;

    printf("lwip!!!\n");

    if (init_ok) {
        rt_kprintf("lwip system already init.\n");
        return 0;
    }

    eth_system_device_init();

    /* set default netif to NULL */
    netif_default = NULL;

    //rc = rt_sem_init(&done_sem, "done", 0, RT_IPC_FLAG_FIFO);
    memset(&cbParam, 0, sizeof(cbParam));
    if (LOS_OK != LOS_BinarySemCreate(0, &cbParam.wait_done)) {
        LWIP_ASSERT("Failed to create semaphore", 0);
        return -1;
    }

    cbParam.ethif = HalGetEthDev();
    tcpip_init(tcpip_init_done_callback, (void *)&cbParam);

    /* waiting for initialization done */
    if (LOS_OK != LOS_SemPend(cbParam.wait_done, LOS_WAIT_FOREVER)) {
        LOS_SemDelete(cbParam.wait_done);
        return -1;
    }
    //rt_sem_detach(&done_sem);
    LOS_SemDelete(cbParam.wait_done);

    /* set default ip address */
#if 0
//#if !LWIP_DHCP
    if (netif_default != RT_NULL)
    {
        struct ip4_addr ipaddr, netmask, gw;

        ipaddr.addr = inet_addr(RT_LWIP_IPADDR);
        gw.addr = inet_addr(RT_LWIP_GWADDR);
        netmask.addr = inet_addr(RT_LWIP_MSKADDR);

        netifapi_netif_set_addr(netif_default, &ipaddr, &netmask, &gw);
    }
#endif
    //rt_kprintf("lwIP-%d.%d.%d initialized!\n", LWIP_VERSION_MAJOR, LWIP_VERSION_MINOR, LWIP_VERSION_REVISION);
    PRINT_ERR("lwIP initialized ...\n");
    init_ok = 1;

    return 0;
}

#endif
