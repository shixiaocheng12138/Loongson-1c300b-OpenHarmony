/*
 * Copyright (c) 2013-2019 Huawei Technologies Co., Ltd. All rights reserved.
 * Copyright (c) 2020-2021 Huawei Device Co., Ltd. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 *    conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list
 *    of conditions and the following disclaimer in the documentation and/or other materials
 *    provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used
 *    to endorse or promote products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef _LOS_ARCH_INTERRUPT_H
#define _LOS_ARCH_INTERRUPT_H

#include "los_config.h"
#include "los_compiler.h"
#include "los_interrupt.h"

#ifdef __cplusplus
#if __cplusplus
extern "C" {
#endif /* __cplusplus */
#endif /* __cplusplus */

#define LS1C_TICK_IRQ   -1
// 中断号
#define LS1C_ACPI_IRQ	0
#define LS1C_HPET_IRQ	1
//#define LS1C_UART00_IRQ	0  // linux中是0，v1.4版本的1c手册中是2，暂屏蔽，待确认
//#define LS1C_UART01_IRQ 1   // 全功能串口0可以分为两个四线串口UART00和UART01
#define LS1C_UART1_IRQ	4
#define LS1C_UART2_IRQ	5
#define LS1C_CAN0_IRQ	6
#define LS1C_CAN1_IRQ	7
#define LS1C_SPI0_IRQ	8
#define LS1C_SPI1_IRQ	9
#define LS1C_AC97_IRQ	10
#define LS1C_MS_IRQ		11
#define LS1C_KB_IRQ		12
#define LS1C_DMA0_IRQ	13
#define LS1C_DMA1_IRQ	14
#define LS1C_DMA2_IRQ   15
#define LS1C_NAND_IRQ	16
#define LS1C_PWM0_IRQ	17
#define LS1C_PWM1_IRQ	18
#define LS1C_PWM2_IRQ	19
#define LS1C_PWM3_IRQ	20
#define LS1C_RTC_INT0_IRQ  21
#define LS1C_RTC_INT1_IRQ  22
#define LS1C_RTC_INT2_IRQ  23
#define LS1C_UART3_IRQ  29
#define LS1C_ADC_IRQ    30
#define LS1C_SDIO_IRQ   31


#define LS1C_EHCI_IRQ	(32+0)
#define LS1C_OHCI_IRQ	(32+1)
#define LS1C_OTG_IRQ    (32+2)
#define LS1C_MAC_IRQ    (32+3)
#define LS1C_CAM_IRQ    (32+4)
#define LS1C_UART4_IRQ  (32+5)
#define LS1C_UART5_IRQ  (32+6)
#define LS1C_UART6_IRQ  (32+7)
#define LS1C_UART7_IRQ  (32+8)
#define LS1C_UART8_IRQ  (32+9)
#define LS1C_UART9_IRQ  (32+13)
#define LS1C_UART10_IRQ (32+14)
#define LS1C_UART11_IRQ (32+15)
#define LS1C_I2C2_IRQ   (32+17)
#define LS1C_I2C1_IRQ   (32+18)
#define LS1C_I2C0_IRQ   (32+19)


#define LS1C_GPIO_IRQ 64
#define LS1C_GPIO_FIRST_IRQ 64
#define LS1C_GPIO_IRQ_COUNT 96
#define LS1C_GPIO_LAST_IRQ  (LS1C_GPIO_FIRST_IRQ + LS1C_GPIO_IRQ_COUNT-1)


#define LS1C_LAST_IRQ 159

// 龙芯1c的中断分为五组，每组32个
#define LS1C_NR_IRQS    (32*5)

#ifndef OS_SYS_VECTOR_CNT
#define OS_SYS_VECTOR_CNT   1
#endif



#ifndef OS_HWI_MAX_NUM
#define OS_HWI_MAX_NUM      LOSCFG_PLATFORM_HWI_LIMIT
#endif

#ifndef OS_VECTOR_CNT
#define OS_VECTOR_CNT       OS_HWI_MAX_NUM + OS_SYS_VECTOR_CNT
#endif

// GPIO编号和中断号之间的互相转换
#define LS1C_GPIO_TO_IRQ(GPIOn)     (LS1C_GPIO_FIRST_IRQ + (GPIOn))
#define LS1C_IRQ_TO_GPIO(IRQn)      ((IRQn) - LS1C_GPIO_FIRST_IRQ)


// 定义中断处理函数
typedef void (*irq_handler_t)(int IRQn, void *param);
typedef struct irq_desc
{
    irq_handler_t   handler;
    void            *param;
}irq_desc_t;

// 初始化异常
void exception_init(void);


/*
 * 使能指定中断
 * @IRQn 中断号
 */
void irq_enable(int IRQn);


/*
 * 禁止指定中断
 * @IRQn 中断号
 */
void irq_disable(int IRQn);


/*
 * 设置中断处理函数
 * @IRQn 中断号
 * @new_handler 新设置的中断处理函数
 * @param 传递给中断处理函数的参数
 * @ret 之前的中断处理函数
 */
irq_handler_t irq_install(int IRQn, irq_handler_t new_handler, void *param);



/* *
 * @ingroup los_arch_interrupt
 * Set interrupt vector table.
 */
 #if (OS_HWI_WITH_ARG == 1)
extern VOID OsSetVector(UINT32 num, HWI_PROC_FUNC vector, VOID *arg);
 #else
extern VOID OsSetVector(UINT32 num, HWI_PROC_FUNC vector);
 #endif


/* *
 * @ingroup  los_arch_interrupt
 * @brief: Hardware interrupt entry function.
 *
 * @par Description:
 * This API is used as all hardware interrupt handling function entry.
 *
 * @attention:
 * <ul><li>None.</li></ul>
 *
 * @param:None.
 *
 * @retval:None.
 * @par Dependency:
 * <ul><li>los_arch_interrupt.h: the header file that contains the API declaration.</li></ul>
 * @see None.
 */
extern VOID HalInterrupt(VOID);

/* *
 * @ingroup  los_arch_interrupt
 * @brief: Get an interrupt number.
 *
 * @par Description:
 * This API is used to get the current interrupt number.
 *
 * @attention:
 * <ul><li>None.</li></ul>
 *
 * @param: None.
 *
 * @retval: Interrupt Indexes number.
 * @par Dependency:
 * <ul><li>los_arch_interrupt.h: the header file that contains the API declaration.</li></ul>
 * @see None.
 */
extern INT32 HalIntNumGet(VOID);

/* *
 * @ingroup  los_arch_interrupt
 * @brief: Default vector handling function.
 *
 * @par Description:
 * This API is used to configure interrupt for null function.
 *
 * @attention:
 * <ul><li>None.</li></ul>
 *
 * @param:None.
 *
 * @retval:None.
 * @par Dependency:
 * <ul><li>los_arch_interrupt.h: the header file that contains the API declaration.</li
></ul>
 * @see None.
 */
 #if (OS_HWI_WITH_ARG == 1)
extern VOID HalHwiDefaultHandler(UINT32 num, VOID *arg);
 #else
extern VOID HalHwiDefaultHandler(VOID);
 #endif

/* *
 * @ingroup  los_arch_interrupt
 * @brief: Reset the vector table.
 *
 * @par Description:
 * This API is used to reset the vector table.
 *
 * @attention:
 * <ul><li>None.</li></ul>
 *
 * @param:None.
 *
 * @retval:None.
 * @par Dependency:
 * <ul><li>los_arch_interrupt.h: the header file that contains the API declaration.</li></ul>
 * @see None.
 */
extern VOID Reset_Handler(VOID);

/* *
 * @ingroup  los_arch_interrupt
 * @brief: Pended System Call.
 *
 * @par Description:
 * PendSV can be pended and is useful for an OS to pend an exception
 * so that an action can be performed after other important tasks are completed.
 *
 * @attention:
 * <ul><li>None.</li></ul>
 *
 * @param:None.
 *
 * @retval:None.
 * @par Dependency:
 * <ul><li>los_arch_interrupt.h: the header file that contains the API declaration.</li></ul>
 * @see None.
 */
extern VOID HalPendSV(VOID);

extern LITE_OS_SEC_TEXT_INIT UINT32 HalOSHwiCreate(INT32 hwiNum,
                                          HWI_PRIOR_T hwiPrio,
                                          HWI_MODE_T mode,
                                          HWI_PROC_FUNC handler,
                                          HWI_ARG_T arg);

#define OS_EXC_IN_INIT                      0
#define OS_EXC_IN_TASK                      1
#define OS_EXC_IN_HWI                       2

#define OS_EXC_MAX_BUF_LEN                  25
#define OS_EXC_MAX_NEST_DEPTH               1

#define OS_NVIC_SHCSR                       0xE000ED24
#define OS_NVIC_CCR                         0xE000ED14

#define OS_NVIC_INT_ENABLE_SIZE             0x20
#define OS_NVIC_INT_PRI_SIZE                0xF0
#define OS_NVIC_EXCPRI_SIZE                 0xC
#define OS_NVIC_INT_CTRL_SIZE               4
#define OS_NVIC_SHCSR_SIZE                  4

#define OS_NVIC_INT_PEND_SIZE               OS_NVIC_INT_ACT_SIZE
#define OS_NVIC_INT_ACT_SIZE                OS_NVIC_INT_ENABLE_SIZE

#define OS_EXC_FLAG_NO_FLOAT                0x10000000
#define OS_EXC_FLAG_FAULTADDR_VALID         0x01
#define OS_EXC_FLAG_IN_HWI                  0x02

#define OS_EXC_IMPRECISE_ACCESS_ADDR        0xABABABAB

#define OS_EXC_EVENT                        0x00000001

/**
 * @ingroup los_exc
 * the struct of register files
 *
 * description: the register files that saved when exception triggered
 *
 * notes:the following register with prefix 'uw'  correspond to the registers in the cpu  data sheet.
 */
typedef struct TagExcContext {
#if ((defined (__FPU_PRESENT) && (__FPU_PRESENT == 1U)) && \
     (defined (__FPU_USED   ) && (__FPU_USED    == 1U))     )
    UINT32 S16;
    UINT32 S17;
    UINT32 S18;
    UINT32 S19;
    UINT32 S20;
    UINT32 S21;
    UINT32 S22;
    UINT32 S23;
    UINT32 S24;
    UINT32 S25;
    UINT32 S26;
    UINT32 S27;
    UINT32 S28;
    UINT32 S29;
    UINT32 S30;
    UINT32 S31;
#endif
    UINT32 uwR4;
    UINT32 uwR5;
    UINT32 uwR6;
    UINT32 uwR7;
    UINT32 uwR8;
    UINT32 uwR9;
    UINT32 uwR10;
    UINT32 uwR11;
    UINT32 uwPriMask;
    /* auto save */
    UINT32 uwSP;
    UINT32 uwR0;
    UINT32 uwR1;
    UINT32 uwR2;
    UINT32 uwR3;
    UINT32 uwR12;
    UINT32 uwLR;
    UINT32 uwPC;
    UINT32 uwxPSR;
#if ((defined (__FPU_PRESENT) && (__FPU_PRESENT == 1U)) && \
     (defined (__FPU_USED) && (__FPU_USED== 1U)))
    UINT32 S0;
    UINT32 S1;
    UINT32 S2;
    UINT32 S3;
    UINT32 S4;
    UINT32 S5;
    UINT32 S6;
    UINT32 S7;
    UINT32 S8;
    UINT32 S9;
    UINT32 S10;
    UINT32 S11;
    UINT32 S12;
    UINT32 S13;
    UINT32 S14;
    UINT32 S15;
    UINT32 FPSCR;
    UINT32 NO_NAME;
#endif
} EXC_CONTEXT_S;

typedef VOID (*EXC_PROC_FUNC)(UINT32, EXC_CONTEXT_S *);
VOID HalExcHandleEntry(UINT32 excType, UINT32 faultAddr, UINT32 pid, EXC_CONTEXT_S *excBufAddr);

/**
 * @ingroup  los_arch_interrupt
 * @brief: Exception initialization.
 *
 * @par Description:
 * This API is used to configure the exception function vector table.
 *
 * @attention:
 * <ul><li>None.</li></ul>
 *
 * @param uwArraySize [IN] Memory size of exception.
 *
 * @retval: None
 * @par Dependency:
 * <ul><li>los_arch_interrupt.h: the header file that contains the API declaration.</li></ul>
 * @see None.
 */
VOID OsExcInit(VOID);

VOID HalExcNMI(VOID);
VOID HalExcHardFault(VOID);
VOID HalExcMemFault(VOID);
VOID HalExcBusFault(VOID);
VOID HalExcUsageFault(VOID);
VOID HalExcSvcCall(VOID);
VOID HalHwiInit();


/**
 * @ingroup los_exc
 * Cortex-M exception types: An error occurred while the bus status register was being pushed.
 */
#define OS_EXC_BF_STKERR           1

/**
 * @ingroup los_exc
 * Cortex-M exception types: An error occurred while the bus status register was out of the stack.
 */
#define OS_EXC_BF_UNSTKERR         2

/**
 * @ingroup los_exc
 * Cortex-M exception types: Bus status register imprecise data access violation.
 */
#define OS_EXC_BF_IMPRECISERR      3

/**
 * @ingroup los_exc
 * Cortex-M exception types: Bus status register exact data access violation.
 */
#define OS_EXC_BF_PRECISERR        4

/**
 * @ingroup los_exc
 * Cortex-M exception types: Bus status register access violation while pointing.
 */
#define OS_EXC_BF_IBUSERR          5

/**
 * @ingroup los_exc
 * Cortex-M exception types: An error occurred while the memory management status register was being pushed.
 */
#define OS_EXC_MF_MSTKERR          6

/**
 * @ingroup los_exc
 * Cortex-M exception types: An error occurred while the memory management status register was out of the stack.
 */
#define OS_EXC_MF_MUNSTKERR        7

/**
 * @ingroup los_exc
 * Cortex-M exception types: Memory management status register data access violation.
 */
#define OS_EXC_MF_DACCVIOL         8

/**
 * @ingroup los_exc
 * Cortex-M exception types: Memory management status register access violation.
 */
#define OS_EXC_MF_IACCVIOL         9


/**
 * @ingroup los_exc
 * Cortex-M exception types: Incorrect usage indicating that the divisor is zero during the division operation.
 */
#define OS_EXC_UF_DIVBYZERO        10

/**
 * @ingroup los_exc
 * Cortex-M exception types: Usage error, error caused by unaligned access.
 */
#define OS_EXC_UF_UNALIGNED        11

/**
 * @ingroup los_exc
 * Cortex-M exception types: Incorrect usage attempting to execute coprocessor related instruction.
 */
#define OS_EXC_UF_NOCP             12

/**
 * @ingroup los_exc
 * Cortex-M exception types: Usage error attempting to load EXC_RETURN to PC illegally on exception return.
 */
#define OS_EXC_UF_INVPC            13

/**
 * @ingroup los_exc
 * Cortex-M exception types: Incorrect usage, attempting to cut to ARM state.
 */
#define OS_EXC_UF_INVSTATE         14

/**
 * @ingroup los_exc
 * Cortex-M exception types: Incorrect usage. Executed instruction whose code is undefined.
 */
#define OS_EXC_UF_UNDEFINSTR       15

/**
 * @ingroup los_exc
 * Cortex-M exception types: NMI
 */

#define OS_EXC_CAUSE_NMI           16

/**
 * @ingroup los_exc
 * Cortex-M exception types: hard fault
 */
#define OS_EXC_CAUSE_HARDFAULT     17

/**
 * @ingroup los_exc
 * Cortex-M exception types: The task handler exits.
 */
#define OS_EXC_CAUSE_TASK_EXIT     18

/**
 * @ingroup los_exc
 * Cortex-M exception types: A fatal error.
 */
#define OS_EXC_CAUSE_FATAL_ERR     19

/**
 * @ingroup los_exc
 * Cortex-M exception types: Hard Fault caused by a debug event.
 */
#define OS_EXC_CAUSE_DEBUGEVT      20

/**
 * @ingroup los_exc
 * Cortex-M exception types: A hard fault that occurs when a quantity is oriented.
 */
#define OS_EXC_CAUSE_VECTBL        21

/**
 * @ingroup los_exc
 * Exception information structure
 *
 * Description: Exception information saved when an exception is triggered on the Cortex-M4 platform.
 *
 */
typedef struct TagExcInfo {
    /**< Exception occurrence phase: 0 means that an exception occurs in initialization, 1 means that an exception occurs in a task, and 2 means that an exception occurs in an interrupt */
    UINT16 phase;
    /**< Exception type. When exceptions occur, check the numbers 1 - 21 listed above */
    UINT16 type;
    /**< If the exact address access error indicates the wrong access address when the exception occurred */
    UINT32 faultAddr;
    /**< An exception occurs in an interrupt, indicating the interrupt number. An exception occurs in the task, indicating the task ID, or 0xFFFFFFFF if it occurs during initialization */
    UINT32 thrdPid;
    /**< Number of nested exceptions. Currently only registered hook functions are supported when an exception is entered for the first time */
    UINT16 nestCnt;
    /**< reserve */
    UINT16 reserved;
    /**< Hardware context at the time an exception to the automatic stack floating-point register occurs */
    EXC_CONTEXT_S *context;
} ExcInfo;


/**
 * @ingroup los_hwi
 * Hardware interrupt error code: Invalid interrupt number.
 *
 * Value: 0x02000900
 *
 * Solution: Ensure that the interrupt number is valid. The value range of the interrupt number applicable
 * for a risc-v platform is [0, OS_RISCV_VECTOR_CNT].
 */
#define OS_ERRNO_HWI_NUM_INVALID                 LOS_ERRNO_OS_ERROR(LOS_MOD_HWI, 0x00)

/**
 * @ingroup los_hwi
 * Hardware interrupt error code: Null hardware interrupt handling function.
 *
 * Value: 0x02000901
 *
 * Solution: Pass in a valid non-null hardware interrupt handling function.
 */
#define OS_ERRNO_HWI_PROC_FUNC_NULL              LOS_ERRNO_OS_ERROR(LOS_MOD_HWI, 0x01)

/**
 * @ingroup los_hwi
 * Hardware interrupt error code: Insufficient interrupt resources for hardware interrupt creation.
 *
 * Value: 0x02000902
 *
 * Solution: Increase the configured maximum number of supported hardware interrupts.
 */
#define OS_ERRNO_HWI_CB_UNAVAILABLE              LOS_ERRNO_OS_ERROR(LOS_MOD_HWI, 0x02)

/**
 * @ingroup los_hwi
 * Hardware interrupt error code: Insufficient memory for hardware interrupt initialization.
 *
 * Value: 0x02000903
 *
 * Solution: Expand the configured memory.
 */
#define OS_ERRNO_HWI_NO_MEMORY                   LOS_ERRNO_OS_ERROR(LOS_MOD_HWI, 0x03)

/**
 * @ingroup los_hwi
 * Hardware interrupt error code: The interrupt has already been created.
 *
 * Value: 0x02000904
 *
 * Solution: Check whether the interrupt specified by the passed-in interrupt number has already been created.
 */
#define OS_ERRNO_HWI_ALREADY_CREATED             LOS_ERRNO_OS_ERROR(LOS_MOD_HWI, 0x04)

/**
 * @ingroup los_hwi
 * Hardware interrupt error code: Invalid interrupt priority.
 *
 * Value: 0x02000905
 *
 * Solution: Ensure that the interrupt priority is valid.
 */
#define OS_ERRNO_HWI_PRIO_INVALID                LOS_ERRNO_OS_ERROR(LOS_MOD_HWI, 0x05)

/**
 * @ingroup los_hwi
 * Hardware interrupt error code: Incorrect interrupt creation mode.
 *
 * Value: 0x02000906
 *
 * Solution: The interrupt creation mode can be only set to ECLIC_NON_VECTOR_INTERRUPT or ECLIC_VECTOR_INTERRUPT of which the
 * value can be 0 or 1.
 */
#define OS_ERRNO_HWI_MODE_INVALID                LOS_ERRNO_OS_ERROR(LOS_MOD_HWI, 0x06)

/**
 * @ingroup los_hwi
 * Hardware interrupt error code: The interrupt has already been created as a fast interrupt.
 *
 * Value: 0x02000907
 *
 * Solution: Check whether the interrupt specified by the passed-in interrupt number has already been created.
 */
#define OS_ERRNO_HWI_FASTMODE_ALREADY_CREATED    LOS_ERRNO_OS_ERROR(LOS_MOD_HWI, 0x07)

/**
 * @ingroup los_hwi
 * Hardware interrupt error code: The API is called during an interrupt, which is forbidden.
 *
 * Value: 0x02000908
 *
 * * Solution: Do not call the API during an interrupt.
 */
#define OS_ERRNO_HWI_INTERR LOS_ERRNO_OS_ERROR(LOS_MOD_HWI, 0x08)

/**
 * @ingroup los_hwi
 * Hardware interrupt error code:the hwi support SHARED error.
 *
 * Value: 0x02000909
 *
 * * Solution:check the input params hwiMode and irqParam of HalHwiCreate or HalHwiDelete whether adapt the current
 * hwi.
 */
#define OS_ERRNO_HWI_SHARED_ERROR LOS_ERRNO_OS_ERROR(LOS_MOD_HWI, 0x09)

/**
 * @ingroup los_hwi
 * Hardware interrupt error code:Invalid interrupt Arg.
 *
 * Value: 0x0200090a
 *
 * * Solution:check the interrupt Arg, Arg should only be ECLIC_LEVEL_TRIGGER, ECLIC_POSTIVE_EDGE_TRIGGER or
 *  ECLIC_NEGTIVE_EDGE_TRIGGER.
 */
#define OS_ERRNO_HWI_ARG_INVALID LOS_ERRNO_OS_ERROR(LOS_MOD_HWI, 0x0a)

/**
 * @ingroup los_hwi
 * Hardware interrupt error code:The interrupt corresponded to the hwi number or devid  has not been created.
 *
 * Value: 0x0200090b
 *
 * * Solution:check the hwi number or devid, make sure the hwi number or devid need to delete.
 */
#define OS_ERRNO_HWI_HWINUM_UNCREATE LOS_ERRNO_OS_ERROR(LOS_MOD_HWI, 0x0b)


extern UINT32 g_curNestCount;
extern UINT32 g_intCount;
extern UINT8 g_uwExcTbl[32];
extern ExcInfo g_excInfo;

#define MAX_INT_INFO_SIZE       (8 + 0x164)

#ifdef __cplusplus
#if __cplusplus
}
#endif /* __cplusplus */
#endif /* __cplusplus */

#endif /* _LOS_ARCH_INTERRUPT_H */
